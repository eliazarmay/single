
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.API_PATH = process.env.API_PATH || '/api';
process.env.SERVER_PORT = process.env.SERVER_PORT || '3000';

require('dotenv').config({
  path: `${__dirname}/../../environments/.env.${process.env.NODE_ENV}`
});

// TODO: Iniciar la conneccion con MongoDB creando un metodo en la clase Utils
// ...