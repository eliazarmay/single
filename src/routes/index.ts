import express, { Express } from 'express';
const Controller = require('../controllers/index');

const router = express.Router();

module.exports = (app: Express) => {
  router.get('/__help__',  Controller.__help__);

  // TODO:  Definir los otros endpoints con sus controladores
  // ...
  
  app.use(`${process.env.API_PATH}/log`, router);
};