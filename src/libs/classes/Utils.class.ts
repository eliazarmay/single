
export default class Utils {
  private static instance: Utils;

  private constructor() { }

  static getInstance() {
    if (!Utils.instance) {
      Utils.instance = new Utils();
    }
    return Utils.instance;
  }
  
  objHasNestedProperty(propertyPath:string, obj: any): ({ isEmpty: boolean, message?: string }) {
    let properties: string[] = propertyPath.split('.');

    for (let i = 0; i < properties.length; i++) {
      let prop: string = properties[i];
      if(!obj || !obj.hasOwnProperty(prop)){
        return { isEmpty: true, message: `'${propertyPath}' is required.\n` };
      }
    }
    return { isEmpty: false };
  }

  getErrorInBodyRequest(propertiesPath:string[], obj: any): [boolean, string] {
    let error: boolean = false,
        errorStr: string = 'Error: ';
    
    propertiesPath.forEach(propertyPath => {
      const { isEmpty, message } = this.objHasNestedProperty(propertyPath, obj);
      if (isEmpty) {
        error = true;
        errorStr += message
      }
    });
    return [error, error ? errorStr : ''];
  }
}