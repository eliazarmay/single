import express from 'express';
require('./config/index');

const app:express.Express = express();
app.use(express.json());

// Routes
app.get('/', (req:express.Request, res:express.Response) => res.json('Hello World'));
require('./routes/index')(app);
 
app.listen(process.env.SERVER_PORT, ()=> console.log(`Iniciado en puerto: ${process.env.SERVER_PORT}`) );