import { Request, Response } from 'express';
import { ApiResult } from "../libs/interfaces/Api.interface";


exports.__help__ = (req: Request, res: Response): Response => {
  const jsonResponse: ApiResult = {
    status:  200,
    message: 'Ok',
    result:  `{ factory: 'Global' | 'RecursosHumanos' | ... | 'Tecnicos', message: <any> }`
  };
  return res.send(jsonResponse);
}; 

// TODO: Definir las funciones principales para insertar en mongodb
// ...